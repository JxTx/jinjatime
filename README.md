# Jijatime

a vulnerable `python` `flask` application to showcase [Server Side Template Injection](https://portswigger.net/research/server-side-template-injection).

## Installation

Its a `flask` application, so you can install the package with `pip`. 

```bash
pip3 install jinjatime --trusted-host 192.168.0.185 --extra-index-url http://__token__:rqvxJpxAgXCiW9yA6K_P@192.168.0.185/api/v4/projects/19/packages/pypi/simple

Looking in indexes: http://192.168.0.185/api/v4/projects/19/packages/pypi/simple, http://192.168.0.185/api/v4/projects/19/packages/pypi/simple
Collecting jinjatime
  Downloading http://192.168.0.185/api/v4/projects/19/packages/pypi/files/5bc75c2c43f5f244d2abc17849210d4dc90175de8050e2162377d9251df7eb29/jinjatime-0.1.0-py3-none-any.whl
Installing collected packages: jinjatime
Successfully installed jinjatime-0.1.0
```

Within `/usr/local/lib/python3.7/dist-packages/jinjatime` run ` python3 ./app.py`. Then browse to [http://127.0.0.1:8000/](http://127.0.0.1:8000/).

![](screenshots/index.png)

## Solutions

Not yet ;) 
