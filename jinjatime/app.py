from datetime import date
from flask import Flask, request, render_template, url_for
from jinja2 import Environment

app = Flask(__name__)
Jinja2 = Environment()


@app.route("/gen_card", methods=['POST'])
def gen_vcard():
    """
    Generates a .vcf file
    """
    name = request.values.get('name')
    if name is None:
        name = "Anonymous"
    org = request.values.get('org')
    phone = request.values.get('phone')
    email = request.values.get('email')
    d = date.today()
    output = Jinja2.from_string("""BEGIN:VCARD
VERSION:2.1
N:"""+(";".join(name.split(" ")))+"""
FN:"""+name+"""
ORG:"""+org+"""
TEL;WORK;VOICE:"""+phone+"""
EMAIL:"""+email+"""
REV:"""+d.isoformat()+"""
END:VCARD""").render()

    return output, 200, {'Content-Disposition': 'attachment; filename='+(name.replace(" ","_"))+".vcf"}


@app.route('/')
def index():
    return render_template('index.html')


@app.route('/hi')
def page():
    """
    % curl 'http://127.0.0.1:8000/page?name={{7*7}}'
    Hello 49!%
    """
    name = request.values.get('name')
    if name is None:
        name = "Anonymous"
    output = Jinja2.from_string('Hello ' + name + '!').render()

    # The variable should be passed to the template context.
    # Jinja2.from_string('Hello {{name}}!').render(name = name)

    return output


if __name__ == "__main__":
    app.run(host='0.0.0.0', port=8000)
